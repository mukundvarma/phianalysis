#include <iostream>
#include "utilities.h"
#include "plotFigure.C"

void mixedbackground(){

TFile *inf = new TFile("outputPhi_opppairs_increasedbckg.root");
TTree *bckgr = (TTree*) inf->Get("bckgr");
TTree *nt = (TTree*) inf->Get("nt");
TCanvas *c1 = new TCanvas ("c1", "c1", 400, 400);

TH1D *hs = new TH1D ("hs", "hs", 100, 0.995, 1.06);
TH1D *hb = new TH1D ("hb", "hb", 100, 0.995, 1.06);
//TH1D *hmass = new TH1D("hmass", "hmass", 100, 0.99, 1.06);
//TH1D *hfinal = new TH1D ("hfinal", "hfinal", 100, 1, 1.06);

bckgr->Draw("phiM >> hb", "pt > 1.9 && pt < 2.9");
nt->Draw("phiM >> hs", "pt > 1.9 && pt < 2.9", "same");
int binmin = hb->FindBin(1.0);
int binmax = hb->FindBin(1.05);
float bmin = hb->GetBinContent(binmin);
float smin = hs->GetBinContent(binmin);
float bmax = hb->GetBinContent(binmax);
float smax = hs->GetBinContent(binmax);
float scaling = (smax+smin)/(bmax+bmin);
hb->Scale(scaling);
hs->SetMarkerSize(0.8);
hs->SetMarkerStyle(20);
hb->SetLineColor(4);
hb->SetLineStyle(2);
 hs->SetMarkerStyle(24);
 hs->SetMarkerColor(kRed);
 hb->SetFillStyle(3004);
 hs->SetStats(0);
 hs->Draw("e");
 hb->Draw("same");
 hs->SetXTitle("M_{KK} (GeV/c^{2})");
 hs->SetYTitle("Entries / (1 MeV/c^{2})");
 hs->GetXaxis()->CenterTitle();
 hs->GetYaxis()->CenterTitle();
 hs->SetTitleOffset(1.65,"Y");
 hs->SetAxisRange(0.985,1.07,"X");
 hs->SetAxisRange(0,hs->GetMaximum()*1.2,"Y");
 
  TLegend *leg = myLegend(0.450,0.72,0.96,0.92);
   leg->AddEntry(hs,"CMS Preliminary","");
   leg->AddEntry(hs,"p+Pb #sqrt{s_{NN}}= 5.02 TeV","pl");
   leg->AddEntry(hs,"1.9-2.9 GeV/c","");
   TLegend *leg2 = myLegend(0.45,0.23,0.9,0.40);
   //leg2->AddEntry(h,"#phi meson p_{T}> 4 GeV/c","");
   leg->SetTextSize(0.04);
   leg->Draw();
   leg2->SetTextSize(0.04);
   leg2->Draw();
/*hmass = hs;
hmass->Add(hb, -1);
hmass->Draw();
*/
}