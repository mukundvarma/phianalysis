#include "utilities.h"
#include "plotFigure.C"




void correlateHMbin1 () {


   // * pPb DATA (25M):
      // * min bias
         // * Forest: /mnt/hadoop/cms/store/user/mvarma/MinBiasForests_Final/Data/pPb_data_2013_minbias_merged.root:
         // * Ntuple: /net/hisrv0001/home/mukund/scratch/Dstar/phianalysis_data_ntupleonly_pPb.root 

      // * high multiplicity (8M)
         // * Forest: /mnt/hadoop/cms/store/user/mvarma/pPb_HIRun2013-PromptReco-v1-HLT_PAPixelTracks_Multiplicity190_v1-forest-v2_24July.root
         // * Ntuple: /net/hisrv0001/home/mukund/scratch/Dstar/phianalysis_data_ntupleonly_HMpPb.root     
      

   // * PbPb DATA (pp re-reco) (15M):
       // * Forest: /mnt/hadoop/cms/store/user/mvarma/PbPb_data_minbias_merged_20July.root 
       // * Ntuple: /net/hisrv0001/home/mukund/scratch/Dstar/phianalysis_data_ntupleonly_PbPb_24july.root
	   
//	   /mnt/hadoop/cms/store/user/mvarma/ntuples/ntuple_pPb_HM, pPbMB, PbPbMB


	TFile *forest = TFile::Open("/mnt/hadoop/cms/store/user/mvarma/pPb_HIRun2013-PromptReco-v1-HLT_PAPixelTracks_Multiplicity190_v1-forest-v2_24July.root");

	TCanvas *canv = new TCanvas("canv", "canv", 900, 900);
	TCanvas *canv2 = new TCanvas("canv2", "canv2", 900, 900);
	canv->Divide(3,3);
	canv2->Divide(3,3);
	TFile *inf = TFile::Open("/net/hisrv0001/home/mukund/scratch/Dstar/phianalysis_data_ntupleonly_HMpPb.root");
	TNtuple *nt = (TNtuple*) inf->Get("nt_all");
	nt->Print();
	// TTree *track = (TTree*) inf->Get("track");
	TTree *track = (TTree*) forest->Get("ppTrack/trackTree");
	track->Print();
    TTree *evt = (TTree*) forest->Get("hiEvtAnalyzer/HiTree"); 
	TFile *outf = new TFile ("plots/corrhists_HMntrk220to260_limstatistics_30july.root", "RECREATE");
	outf->cd();
	int ntentries = nt->GetEntries();
	double oldpt = 0.0;
	double newpt;
	float pi = 3.14;
	float deltaeta;
	float deltaphi;
	float deltaetabg;
	float deltaphibg;
	float rangemin, rangemax;
	
	float mass[9]={1019.06, 1019.48, 1019.61, 1019.38, 1019.57, 1019.64, 1019.58, 1020.02, 1019.04};
	float sigmamass[9]={0.09,0.1, 0.11, 0.12,  0.14, 0.15, 0.27, 0.35, 0.3};
	
		
	float phi_Phi;
	float phi_Eta;
	float phi_K1;
	float phi_K2;
	float eta_K1;
	float eta_K2;
	
	float phiM;
	float phiPt;
	float phiPhi;
	float phiEta;
	float pt1;
	float pt2;
	float eta1;
	float eta2;
	float phi1;
	float phi2;
	float vz;
	float evtentry;
	int nt_nEv;
	int counter;
	int ntrig=0;
	int hiNtracks;
	
	float trkPt[10000]={0};
	float trkEta[10000]={0};
	float trkPhi[10000]={0};
	int nTrk;
	bool highPurity[10000]={0};
	
	const int nbins = 11;
	double bins[nbins+1] = {0.9, 1.4, 1.9, 2.4, 2.9, 3.4, 3.9, 4.4, 4.9, 5.4, 5.9, 6.4};
	
	TNtuple *pndata = new TNtuple ("pndata", "", "pTbin:p0:p1:p2:p3:p0relerr:p1relerr:p2relerr:p3relerr");
	
	// const int nbins = 3;
	// double bins[nbins+1] = {1.9, 2.9,  4.7, 19.9};
	
	TH2D *hist_all[nbins];
	TH2D *hist_bg[nbins];
	TH2D *hist_final[nbins];
	TH1D *hist_proj[nbins];
	float bg[nbins]={0};
	TF1* fit[nbins];
	double p0[nbins]={0};
	double p1[nbins]={0};
	double p2[nbins]={0};
	double p3[nbins]={0};
	double p0err[nbins]={0};
	double p2err[nbins]={0};
	double v2[nbins]={0};
	double v2err[nbins]={0};
	TH1D *v2hist = new TH1D ("v2hist", "", nbins, bins);
	
	nt->SetBranchAddress("phiM", &phiM);
    nt->SetBranchAddress("phiPt", &phiPt);
    nt->SetBranchAddress("phiEta", &phiEta);
    nt->SetBranchAddress("phiPhi", &phiPhi);
	nt->SetBranchAddress("pt1", &pt1);
    nt->SetBranchAddress("eta1", &eta1);
    nt->SetBranchAddress("phi1", &phi1);
	nt->SetBranchAddress("pt2", &pt2);
    nt->SetBranchAddress("eta2", &eta2);
    nt->SetBranchAddress("phi2", &phi2);
	nt->SetBranchAddress("vz", &vz);
	nt->SetBranchAddress("evtentry", &evtentry);
	
	track->SetBranchAddress("trkPt", trkPt);
	track->SetBranchAddress("trkEta", trkEta);
	track->SetBranchAddress("trkPhi", trkPhi);
	track->SetBranchAddress("nTrk", &nTrk);
	track->SetBranchAddress("highPurity", highPurity);
	
	evt->SetBranchAddress("hiNtracks", &hiNtracks);
	
	for (int h = 0;h <nbins; h++)
	{
		hist_bg[h] = new TH2D(Form("hist_bg%d",h) ,"background;#Delta#eta;#Delta#phi; Events",27,-4.1,4,32,-pi/2,3*pi/2);
		hist_all[h] = new TH2D(Form("hist_all%d",h) ,"delta eta vs delta phi;#Delta#eta;#Delta#phi; Events",27,-4.1,4,32,-pi/2,3*pi/2);
		fit[h]= new TF1(Form("fit%d",h), "[0]*(1+[1]*2*cos(x) + [2]*2*cos(2*x)+[3]*2*cos(3*x)+[4]*2*cos(4*x)+[5]*2*cos(5*x))");

	}
	
	int hindex=0;
		
	for (int k = 0; k < 50000000; k++) {
		nt->GetEntry(k);
		if (k%100000==0) cout << "Ntuple Entry: " << k << endl;
		nt_nEv=static_cast<int>(evtentry);
		
		
		phi_Phi = phiPhi;
		phi_Eta = phiEta;
		eta_K1 = eta1;
		eta_K2 = eta2;
		phi_K1 = phi1;
		phi_K2 = phi2;
	    if (phiPt > 6.4) continue; 
		// out << "PhiPT: " << phiPt << endl;
		if (phiPt < 0.9) continue; 
		// cout << "PhiPT: " << phiPt << endl;
		for (int i = 0; i < nbins;i++) { 
		if (phiPt >= bins[i] && phiPt < bins[i+1]) hindex = i;
		}
		if (phiM > 1.03) continue;
		if (phiM < 1.01) continue;
		ntrig++;
		evt->GetEntry(nt_nEv);
		if (hiNtracks < 220) continue;
		if (hiNtracks > 260) continue;
		track->GetEntry(nt_nEv);
		// cout << "Track Entry: " << nt_nEv << endl;
		//Signal
		for (int j = 0;j < nTrk; j++) {
		if (trkPt[j] > 3.0) continue;
		if (trkPt[j] < 0.5) continue;
		if (!(highPurity[j])) continue;
		if (TMath::Abs(trkEta[j]) > 2.4) continue;
		 if (TMath::Abs(phi_K1 - trkPhi[j]) < 0.0001 && TMath::Abs(eta_K1 - trkEta[j]) < 0.0001) continue;
		//{cout << "daughter1, ";continue;}
		 if (TMath::Abs(phi_K2 - trkPhi[j]) < 0.0001 && TMath::Abs(eta_K2 - trkEta[j]) < 0.0001) continue;
		//{cout << "daughter2" << endl;continue;}
		
			deltaeta = TMath::ACos(TMath::Cos(phi_Eta-trkEta[j]));
			deltaphi = TMath::ACos(TMath::Cos(phi_Phi-trkPhi[j]));
			//deltaeta2 = TMath::Abs(phi_Eta-eta2);
			//deltaphi2 = TMath::Abs(phi_Phi-phi2);
// cout << "Delta phi: " << deltaphi << "Delta eta: " << deltaeta << endl;

			if  (deltaphi < pi/2)//corr
			{
				hist_all[hindex]->Fill(deltaeta, deltaphi);
				hist_all[hindex]->Fill(-1*deltaeta, deltaphi);
				hist_all[hindex]->Fill(deltaeta, -1*deltaphi);
				hist_all[hindex]->Fill(-1*deltaeta, -1*deltaphi);
			}
			else if (deltaphi < 1.5*pi)//corr
			{
				hist_all[hindex]->Fill(deltaeta, deltaphi);
				hist_all[hindex]->Fill(-1*deltaeta, deltaphi);
				hist_all[hindex]->Fill(deltaeta, 2*pi - deltaphi);
				hist_all[hindex]->Fill(-1*deltaeta, 2*pi - deltaphi);
			}
			else 
			{
				hist_all[hindex]->Fill(deltaeta, 2*pi - deltaphi);
				hist_all[hindex]->Fill(-1*deltaeta, 2*pi - deltaphi);
				hist_all[hindex]->Fill(deltaeta, -2*pi + deltaphi);		
				hist_all[hindex]->Fill(-1*deltaeta, -2*pi + deltaphi);
			}			
		}
		// if (nt_nEv%10000 == 0)
		// cout << "NEvent: " << evtentry << endl;
		//Background
		for (int b = 0;b < 10;b++) {
		track->GetEntry(nt_nEv+b);
		for (int j = 0;j < nTrk; j++) {
			if (trkPt[j] > 3.0) continue;
			if (trkPt[j] < 0.5) continue;
			if (!(highPurity[j])) continue;
			if (TMath::Abs(trkEta[j]) > 2.4) continue;
		//	if (TMath::Abs(phi_K1 - trkPhi[j]) < 0.0001 && TMath::Abs(eta_K1 - trkEta[j]) < 0.0001) {cout << "daughter1, ";continue;}
		//	if (TMath::Abs(phi_K2 - trkPhi[j]) < 0.0001 && TMath::Abs(eta_K2 - trkEta[j]) < 0.0001) {cout << "daughter2" << endl;continue;}
		
			deltaetabg = TMath::ACos(TMath::Cos(phi_Eta-trkEta[j]));
			deltaphibg = TMath::ACos(TMath::Cos(phi_Phi-trkPhi[j]));
			//deltaeta2 = TMath::Abs(phi_Eta-eta2);
			//deltaphi2 = TMath::Abs(phi_Phi-phi2);		
			
			if  (deltaphibg < pi/2)//corr
			{
				hist_bg[hindex]->Fill(deltaetabg, deltaphibg);
				hist_bg[hindex]->Fill(-1*deltaetabg, deltaphibg);
				hist_bg[hindex]->Fill(deltaetabg, -1*deltaphibg);
				hist_bg[hindex]->Fill(-1*deltaetabg, -1*deltaphibg);
			}
			else if (deltaphi < 1.5*pi)//corr
			{
				hist_bg[hindex]->Fill(deltaetabg, deltaphibg);
				hist_bg[hindex]->Fill(-1*deltaetabg, deltaphibg);
				hist_bg[hindex]->Fill(deltaetabg, 2*pi - deltaphibg);
				hist_bg[hindex]->Fill(-1*deltaetabg, 2*pi - deltaphibg);
			}
			else 
			{
				hist_bg[hindex]->Fill(deltaetabg, 2*pi - deltaphibg);
				hist_bg[hindex]->Fill(-1*deltaetabg, 2*pi - deltaphibg);
				hist_bg[hindex]->Fill(deltaetabg, -2*pi + deltaphibg);		
				hist_bg[hindex]->Fill(-1*deltaetabg, -2*pi + deltaphibg);
			}			
		}
		
		}
		
	}
	for (int h =0;h<nbins;h++){
	
	canv->cd(h+1); 
	gPad->SetTheta(60.849);
	gPad->SetPhi(38.0172);
	bg[h]=hist_bg[h]->GetMaximum();
	hist_final[h] = (TH2D*) hist_all[h]->Clone (Form("hist_final%d", h));
	hist_final[h]->Sumw2();
	hist_bg[h]->Sumw2();
	hist_final[h]->Divide(hist_bg[h]);
	hist_final[h]->GetYaxis()->SetRangeUser(-0.5*3.1415, 1.5*3.1415);
	hist_final[h]->GetXaxis()->SetRangeUser(-3.6, 3.6);
	hist_final[h]->Draw("surf1a");
	hist_final[h]->Scale(bg[h]);
	hist_final[h]->Scale(1./ntrig);


	
	 
	
	 rangemin=hist_all[h]->GetXaxis()->FindBin(1.5);
	 rangemax=hist_all[h]->GetXaxis()->FindBin(4);
	//Taking projections
	canv2->cd(h+1);
	hist_proj[h] = hist_final[h]->ProjectionY(Form("Projection%d",h), rangemin,rangemax,"d");
	hist_proj[h]->GetXaxis()->SetRangeUser(0,3.1415);
	hist_proj[h]->Draw();
	hist_proj[h]->Fit(Form("fit%d", h));
	
	p0[h]=fit[h]->GetParameter(0)*1.;
	p1[h]=fit[h]->GetParameter(1)*1.;
	p2[h]=fit[h]->GetParameter(2)*1.;
	p3[h]=fit[h]->GetParameter(3)*1.;
	p0err[h]=fit[h]->GetParError(0)*1./p0[h];
	p2err[h]=fit[h]->GetParError(2)*1./p2[h];
	pndata->Fill((bins[h]+bins[h+1])/2, fit[h]->GetParameter(0)*1., fit[h]->GetParameter(1)*1., fit[h]->GetParameter(2)*1., fit[h]->GetParameter(3)*1., fit[h]->GetParError(0)*1./p0[h], fit[h]->GetParError(1)*1./p1[h], fit[h]->GetParError(2)*1./p2[h], fit[h]->GetParError(3)*1./p3[h]);
	v2[h]=sqrt(p2[h]);
	v2err[h]=v2[h]*p2err[h]/2;
	v2hist->SetBinContent(h, v2[h]);
	v2hist->SetBinError(h, v2err[h]);
	// TPaveText *pt1 = new TPaveText(0,0.8,0.4,1, "NDC"); 
	// pt1->SetFillColor(0); 
	// pt1->SetTextSize(0.04); 
	// pt1->SetTextAlign(12);
	// pt1->AddText(text);
	// pt1->AddText(" 1.5 < #eta < 4");
	// pt1->Draw(); 
		canv->Update();
		canv2->Update();
	}
	// hist_final[0]->SaveAs("histfinal1.gif");
	// hist_final[1]->SaveAs("histfinal2.gif");
	// hist_final[2]->SaveAs("histfinal3.gif");
	// hist_final[3]->SaveAs("histfinal4.gif");
	// hist_final[4]->SaveAs("histfinal5.gif");
	// hist_final[5]->SaveAs("histfinal6.gif");
	// hist_final[6]->SaveAs("histfinal7.gif");
	// hist_final[7]->SaveAs("histfinal8.gif");
	// hist_final[8]->SaveAs("histfinal9.gif");
	// hist_proj[0]->SaveAs("histproj1.gif");
	// hist_proj[1]->SaveAs("histproj2.gif");
	// hist_proj[2]->SaveAs("histproj3.gif");
	// hist_proj[3]->SaveAs("histproj4.gif");
	// hist_proj[4]->SaveAs("histproj5.gif");
	// hist_proj[5]->SaveAs("histproj6.gif");
	// hist_proj[6]->SaveAs("histproj7.gif");
	// hist_proj[7]->SaveAs("histproj8.gif");
	// hist_proj[8]->SaveAs("histproj9.gif");
	// p2->Write();
	// v2->Write();
	// p2err->Write();
	// v2err->Write();
	v2hist->Draw();
	v2hist->Write();
	pndata->Write();
	outf->Write();
	outf->Close();
	cout << "HM 220 - 260 lim";
}




	