#include<iostream>
#include"plotFigure.C"
#include"utilities.h"

void thetastar(){

TFile *f = new TFile("outputPhi_MC.root");
TTree *nt = (TTree*)f->Get("nt");
TCanvas *c1 = new TCanvas ("c1", "c1", 700, 320);

TH1D *hist1 = new TH1D ("hist1", "Cos #theta*;Cos #theta*;N",50 ,-2.5, 2.5);
TH1D *hist2 = new TH1D ("hist2", "Cos #theta*;Cos #theta*;N",50 ,-2.5, 2.5);
 
c1->Divide(2, 1);
c1->cd(1);
nt->Draw("TMath::Cos(thetastar)>>hist1", "phiM > 0", "p");
TLegend *leg = myLegend(0.60,0.72,0.90,0.92);
leg->AddEntry(hist1,"CMS Preliminary","");
leg->AddEntry(hist1,"p+Pb #sqrt{s_{NN}}= 5.02 TeV","pl");
leg->AddEntry(hist1,"complete Phi Mass range","");
leg->Draw();
 
c1->cd(2);
nt->Draw("TMath::Cos(thetastar)>>hist2", "phiM > 1.01 && phiM < 1.03", "p"); 
TLegend *leg2 = myLegend(0.60,0.72,0.90,0.92);
leg2->AddEntry(hist1,"CMS Preliminary","");
leg2->AddEntry(hist1,"p+Pb #sqrt{s_{NN}}= 5.02 TeV","pl");
leg2->AddEntry(hist1,"1.03 GeV > M_{#phi} > 1.01 GeV","");
leg2->Draw();

}