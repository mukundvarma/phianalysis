{
//========= Macro generated from object: hv2_sb1/Sideband region #mu +/- 1.5 #Gamma to #mu +/- 3.2 #Gamma
//========= by ROOT version5.32/00
   Double_t xAxis1[8] = {0, 1.5, 2, 2.5, 3, 4, 5, 6}; 
   
   TH1D *hv2_sb1 = new TH1D("hv2_sb1","Sideband region #mu +/- 1.5 #Gamma to #mu +/- 3.2 #Gamma",7, xAxis1);
   hv2_sb1->SetBinContent(2,0.0401618);
   hv2_sb1->SetBinContent(3,0.11164);
   hv2_sb1->SetBinContent(4,0.143755);
   hv2_sb1->SetBinContent(5,0.194166);
   hv2_sb1->SetBinContent(6,0.183931);
   hv2_sb1->SetBinContent(7,0.242783);
   hv2_sb1->SetBinError(2,0.0320945);
   hv2_sb1->SetBinError(3,0.020604);
   hv2_sb1->SetBinError(4,0.0157986);
   hv2_sb1->SetBinError(5,0.0130425);
   hv2_sb1->SetBinError(6,0.0133332);
   hv2_sb1->SetBinError(7,0.0187344);
   hv2_sb1->SetMinimum(0);
   hv2_sb1->SetMaximum(0.3);
   hv2_sb1->SetEntries(6);
   hv2_sb1->SetStats(0);
   hv2_sb1->SetFillColor(1);
   hv2_sb1->SetFillStyle(0);
   hv2_sb1->SetLineStyle(0);

   Int_t ci;   // for color index setting
   ci = TColor::GetColor("#0000ff");
   hv2_sb1->SetMarkerColor(ci);
   hv2_sb1->SetMarkerStyle(20);
   hv2_sb1->GetXaxis()->SetLabelFont(42);
   hv2_sb1->GetXaxis()->SetLabelOffset(0.007);
   hv2_sb1->GetXaxis()->SetLabelSize(0.05);
   hv2_sb1->GetXaxis()->SetTitleSize(0.06);
   hv2_sb1->GetXaxis()->SetTitleOffset(0.9);
   hv2_sb1->GetXaxis()->SetTitleFont(42);
   hv2_sb1->GetYaxis()->SetLabelFont(42);
   hv2_sb1->GetYaxis()->SetLabelOffset(0.007);
   hv2_sb1->GetYaxis()->SetLabelSize(0.05);
   hv2_sb1->GetYaxis()->SetTitleSize(0.06);
   hv2_sb1->GetYaxis()->SetTitleOffset(1.25);
   hv2_sb1->GetYaxis()->SetTitleFont(42);
   hv2_sb1->GetZaxis()->SetLabelFont(42);
   hv2_sb1->GetZaxis()->SetLabelOffset(0.007);
   hv2_sb1->GetZaxis()->SetLabelSize(0.05);
   hv2_sb1->GetZaxis()->SetTitleSize(0.06);
   hv2_sb1->GetZaxis()->SetTitleFont(42);
   hv2_sb1->Draw("");
}
