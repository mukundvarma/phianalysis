//Define Input Forest
//Define Output Histograms, Files and Ntuples
//Define Constant Variables
//Define Variables
//Define Mass and Width
//Define Forest branches
float pi = 3.14159;
const int nptbins=9;
const int nvzbins =15;
int bgscorekeeper=0;

TLorentzVector Kcand1;
TLorentzVector Kcand2;
TLorentzVector Phicand;
TLorentzVector Phisforbgmixing[nvzbins][10];

TH2D *FillCorrHist(TH2D *hist, deta, dphi)
{
  if (dphi >= pi) dphi = 2*pi - phi;
  hist->Fill(deta, dphi);
  hist->Fill(deta, -dphi);
  hist->Fill(-deta, dphi);
  hist->Fill(-deta, -dphi);
  hist->Fill(deta, 2*pi - dphi);
  hist->Fill(-deta, 2*pi - dphi);
  return hist;
}

void correlation()
{
  TH2D* hPhiTrackSignal[2][nptbins];//[0]  = mass candidate band, [1] = side band
  TH2D* hPhiTrackBG[2][nptbins]; // 
  TH2D* hTrackTrackSignal[nptbins];
  TH2D* hTrackTrackBG[nptbins];
 
  int nPhi_PhiTrackSignal[2][nptbins];//[0]  = mass candidate band, [1] = side band
  int nPhi_PhiTrackBG[2][nptbins]; // 
  int ntrk_TrackTrackSignal[nptbins];
  int ntrk_TrackTrackBG[nptbins];
  
  TH1D *hPhiMass[nptbins];
}
 
bool myTrackQuality(int j) 
{ 
  bool mytrkquality = false;
  if (fabs(trkPt[j])>0.5)&&trkEta[j]<2.4&&trkAlgo[j]<4&&highpurity[j]&&(fabs(trkDz1[j]/trkDzError1[j])<3&&fabs(trkDxy1[j]/trkDxyError1[j])<3&&fabs(trkPtError[j]/trkPt[j]<0.1))
  mytrkquality = true;
  return mytrkquality
}
 
void GetPhi() {
  if (myTrackQuality(j) == false) continue;
  for (int k=j+1;k<nTrk;k++) {	
    if (myTrackQuality(k) == false) continue;
	if (trkCharge[k]!= (-1)*trkCharge[j]) continue; // K+ K- should be of opposite sign
	Kcand1.SetPtEtaPhiM(trkPt[j],trkEta[j],trkPhi[j],0.493677);
	Kcand2.SetPtEtaPhiM(trkPt[k],trkEta[k],trkPhi[k],0.493677);
	Phicand = Kcand1 + Kcand2;
	hPhiMass->Fill(Phicand.M());
	int vzbin = static_cast<int>((vz[j]+15)/2);
	if (vzbin > 14) vzbin = 14; if (vzbin < 0) vzbin =0;
	for (int m = 0;m < nTrk; m++)
	{
	  if (m==k || m ==j) continue;
      deta_PhiTrack = TMath::ACos(TMath::Cos(Phicand.Eta()-trkEta[m]));
	  dphi_PhiTrack = TMath::ACos(TMath::Cos(Phicand.Phi()-trkPhi[m]));
	  for (int bincntr = 0; bincntr < nptbins; bincntr++) { 
		if (Phicand.Pt() >= nptbins[i] && Phicand.Pt < nptbins[i+1]) ptbin = bincntr;
	  }
	  if (Phicand.M() <= phiMass[ptbin] + phiWidth[ptbin] && Phicand.M() >= phiMass[ptbin] - phiWidth[ptbin]) {  
	    FillCorrHist(hPhiTrackSignal[0][ptbin], deta_PhiTrack, dphi_PhiTrack);
	    nPhi_PhiTrackSignal[0][ptbin]++;
	  }
	  if (Phicand.M() <= phiMass[ptbin] + 4*phiWidth[ptbin] && Phicand.M() >= phiMass[ptbin] + 2*phiWidth[ptbin]) {
	    FillCorrHist(hPhiTrackSignal[1][ptbin], deta_PhiTrack, dphi_PhiTrack);
	    nPhi_PhiTrackSignal[1][ptbin]++;
	  }
	  if (Phicand.M() <= phiMass[ptbin] - 2*phiWidth[ptbin] && Phicand.M() >= phiMass[ptbin] - 4*phiWidth[ptbin]) {
	    FillCorrHist(hPhiTrackSignal[1][ptbin], deta_PhiTrack, dphi_PhiTrack);
	    nPhi_PhiTrackSignal[1][ptbin]++;
	  }
	Phisforbgmixing[vzbin][bgscorekeeper] = Phicand;
	if (bgscorekeeper == 14) bgscorekeeper = 0;
	else bgscorekeeper++;
    	

			 
 
 
 