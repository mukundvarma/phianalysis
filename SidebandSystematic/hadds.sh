tag=$1
cd ~mukund/scratch/corrhists/"$tag"
mkdir -p "merged"
mult=( 120 150 185 220 260 )
for i in 0 1 2 3
  do
    low=${mult[$i]}
    high=${mult[$i+1]}
    echo $low " | " $high
    hadd -f ~mukund/scratch/corrhists/"$tag"/merged/$1_nmin"$low"_nmax"$high".root ~mukund/scratch/corrhists/"$tag"/nmin"$low"_nmax"$high"/*.root
  done
