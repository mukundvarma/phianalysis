if [ $# -ne 5 ]
then
  echo "Usage: ./submit.sh <trackqual> <file-list> <tag> <nmin> <nmax>"
  exit 1
fi

now="submission/$3_$(date +"%Y_%m_%d__%H_%M_%S")"
len=`wc -l $2 | awk '{print $1}'`
njobs=$((len))

mkdir $now
cp $2 $now
cp runspectra.sh $now
cat runspectra.condor | sed "s@log_flag@$now@g" | sed "s@dir_flag@$PWD/$now@g" |  sed "s@arglist@$1 $2 $3 $4 $5 $6@g" | sed "s@transfer_filelist@$2,runphispectra.exe@g" | sed "s@njobs@$njobs@g" > $now/runspectra.condor

NAME="runphispectra.C"
g++ $NAME $(root-config --cflags --libs) -Werror -Wall -O2 -o "${NAME/%.C/}.exe"
cp runphispectra.exe $now
echo
cat $now/runspectra.condor
echo 
echo condor_submit $now/runspectra.condor
echo
#condor_submit $now/runspectra.condor