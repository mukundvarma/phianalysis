// #include "/net/hisrv0001/home/dav2105/run/CMSSW_4_4_4/src/CmsHi/JetAnalysis/macros/forest/hiForest_charge.h"
#include "/net/hisrv0001/home/mukund/HiForestAnalysis/HiForestAnalysis/hiForest.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <iostream>
#include <math.h>

// #include "/net/hisrv0001/home/mukund/scratch/Dstar/utilities.h"
#include <TLorentzVector.h>


using namespace std;

const double mass_K = 0.493677;
const double pi = TMath::Pi();
const double phimass_pdg = 1.019455;
const double phiwidth_pdg = 0.00426;
int ntottrig = 0;
//Cuts
const int maxnhits = 5;
const double etamax = 2.4;
const int vzmax = 15;

int mytrackquality = 0;

// int evtplanechooser = 1;

//TH1D * h1mult;
// TH2D * h2Phimass;
TH3D *h3PhiTrackBackground;
TH3D *h3Phiptetaphi;
TH3D *h3Phiptetaphi_gen;
TH1D *h1Phimass;
TH1D *h1nevents;
TH1D *hnphis;
TH1D *hntrks;

TH1D *h1genPhigenevtplane_sig;
TH1D *h1genPhigenevtplane_bkg;

TH1D *h1recoPhigenevtplane_sig;
TH1D *h1recoPhigenevtplane_bkg;

TH1D *h1genPhirecoevtplane_sig;
TH1D *h1genPhirecoevtplane_bkg;

TH1D *h1recoPhirecoevtplane_sig;
TH1D *h1recoPhirecoevtplane_bkg;

TH1D *h1gentrkgenevtplane_sig;
TH1D *h1gentrkgenevtplane_bkg;

TH1D *h1recotrkgenevtplane_sig;
TH1D *h1recotrkgenevtplane_bkg;

TH1D *h1gentrkrecoevtplane_sig;
TH1D *h1gentrkrecoevtplane_bkg;

TH1D *h1recotrkrecoevtplane_sig;
TH1D *h1recotrkrecoevtplane_bkg;


TH2D *h2genPhisig_twopart;
TH2D *h2genPhibkg_twopart;

TH2D *h2gentrksig_twopart;
TH2D *h2gentrkbkg_twopart;

TH2D *h2TrackTrackSignal;
TH2D *h2TrackTrackBackground;


HiForest *c;

TH3D * hPhiCorrSignal(int nmin, int nmax, double ptmin, double  ptmax);
// TH3D * hPhiCorrBackground(int nmin, int nmax,double ptmin, double ptmax);
bool Checktracks(double tracketa, double trackpt, double trackpterror, double dz, double dzerror, double dxy, double dxyerror, int nhits, bool highpurity);

int GetNTotTrig();
void FillHist2D(TH2D *hist, double deltaeta, double deltaphi, double weight);
void FillHist3D(TH3D *hist, double Phicandpt, double deltaeta,  double deltaphi, double weight);

void corrana(const char * infname = "/mnt/hadoop/cms/store/user/velicanu/mergedv1_sortedforest/mergesortv1_0.root", int trackquality = 0)
{
	mytrackquality = trackquality;
	cout<<"initializing hiforest and building centrality index"<<endl;
	cout<<"running on: "<<infname<<endl;
	c = new HiForest(infname,"forest",cPPb, 1);
	c->LoadNoTrees();
	c->hasTrackTree = true;
	c->hasEvtTree = true;
	c->hasGenParticleTree = true;
}

bool skipevent(double vzrange, int runboundary)
{
	bool doskip = false;
	if(fabs(c->evt.vz)>vzrange) doskip = true;
	if(c->evt.run>runboundary) doskip = true;
	return doskip;
}


TH3D *hPhiCorrSignal(int nmin, int nmax, double ptmin, double ptmax)//gen=0, reco=1
{
	string PIDtag;
	Long64_t nentries = c->GetEntries();
	
	TLorentzVector Kcand1, Kcand2, Phicand;
	// Long64_t nentries = 800;

	PIDtag = "reco";
	
	TH3D *h3PhiTrackSignal = new TH3D(Form("h3sig_mdetadphi_phitrk_trig%d_%d_mult%d_%d_%s", (int) ptmin, (int) ptmax, nmin, nmax, PIDtag.data()),  ";M_{KK};#Delta#eta;#Delta#phi", 240, 0.98, 1.1, 40, -5, 5, 48, -pi, 2 * pi);
	h3PhiTrackBackground = new TH3D(Form("h3bkg_mdetadphi_phitrk_trig%d_%d_mult%d_%d_%s", (int) ptmin, (int) ptmax, nmin, nmax, PIDtag.data()),  ";M_{KK};#Delta#eta;#Delta#phi", 240, 0.98, 1.1, 40, -5, 5, 48, -pi, 2 * pi);
	
	//    h1mult    = new TH1D(Form("h1mult_mult%d_%d", nmin, nmax),";N",300,0,300);
	h3Phiptetaphi = new TH3D(Form("h3_ptetaphi_Phidistr_ptmin%d_ptmax%d_pdgmasspmgamma_mult%d_%d_%s",(int)ptmin, (int)ptmax, nmin, nmax, PIDtag.data()), "Phicand distribution;p_{T};#Delta#eta;#Delta#phi",  40, 0.0, 20.0, 40, -5, 5, 48, -pi, 2 * pi);    
	h3Phiptetaphi_gen = new TH3D(Form("h3_ptetaphi_Phidistr_ptmin%d_ptmax%d_pdgmasspmgamma_mult%d_%d_%s",(int)ptmin, (int)ptmax, nmin, nmax, "gen"), "Phicand distribution;p_{T};#Delta#eta;#Delta#phi",  40, 0.0, 20.0, 40, -5, 5, 48, -pi, 2 * pi);    
	h1nevents = new TH1D(Form("h1_nevents_ptmin%d_ptmax%d_nmin%d_nmax%d_%s", (int) ptmin, (int) ptmax, nmin, nmax, PIDtag.data()), "", 1,0,1);
	h1Phimass = new TH1D(Form("h1_Phimass_phi%d_%d_nmin%d_%d_%s", (int) ptmin, (int) ptmax, nmin, nmax, PIDtag.data()),  "Phi Mass;M_{KK} (GeV/c^{2});pT;Entries / (1 MeV/c^{2})", 240, 0.98, 1.1);
	hnphis = new TH1D(Form("h1_nphis_ptmin%d_ptmax%d_nmin%d_%d_%s", (int) ptmin, (int) ptmax, nmin, nmax, PIDtag.data()), "", 1,0,1);
	hntrks = new TH1D(Form("h1_ntrks_ptmin%d_ptmax%d_nmin%d_%d_%s", (int) ptmin, (int) ptmax, nmin, nmax, PIDtag.data()), "", 1,0,1);
	
	h2genPhisig_twopart = new TH2D(Form("h2_phitrkcorr_trig%d_%d_mult%d_%d_twopart_sig", (int) ptmin, (int) ptmax, nmin, nmax), ";#Delta#eta;#Delta#phi", 40, -5, 5, 48, -pi, 2*pi);
	h2genPhibkg_twopart = new TH2D(Form("h2_phitrkcorr_trig%d_%d_mult%d_%d_twopart_bkg", (int) ptmin, (int) ptmax, nmin, nmax), ";#Delta#eta;#Delta#phi", 40, -5, 5, 48, -pi, 2*pi);
	
	h2gentrksig_twopart = new TH2D(Form("h2_gen_trktrkcorr_trig%d_%d_mult%d_%d_sig", (int) ptmin, (int) ptmax, nmin, nmax), ";#Delta#eta;#Delta#phi", 40, -5, 5, 48, -pi, 2*pi);
	h2gentrkbkg_twopart = new TH2D(Form("h2_gen_trktrkcorr_trig%d_%d_mult%d_%d_bkg", (int) ptmin, (int) ptmax, nmin, nmax), ";#Delta#eta;#Delta#phi", 40, -5, 5, 48, -pi, 2*pi);
	
	h2TrackTrackSignal = new TH2D(Form("h2_reco_trktrkcorr_trig%d_%d_mult%d_%d_sig", (int) ptmin, (int) ptmax, nmin, nmax), ";#Delta#eta;#Delta#phi", 40, -5, 5, 48, -pi, 2*pi);
	h2TrackTrackBackground = new TH2D(Form("h2_reco_trktrkcorr_trig%d_%d_mult%d_%d_bkg", (int) ptmin, (int) ptmax, nmin, nmax), ";#Delta#eta;#Delta#phi", 40, -5, 5, 48, -pi, 2*pi);
	
	h1genPhigenevtplane_sig = new TH1D(Form("h1_genphigenevt_trig%d_%d_mult%d_%d_evtplane_sig", (int) ptmin, (int) ptmax, nmin, nmax), ";#Delta#phi", 48, -pi, 2*pi);
	h1genPhigenevtplane_bkg = new TH1D(Form("h1_genphigenevt_trig%d_%d_mult%d_%d_evtplane_bkg", (int) ptmin, (int) ptmax, nmin, nmax), ";#Delta#phi", 48, -pi, 2*pi);
	
	h1genPhirecoevtplane_sig = new TH1D(Form("h1_genphirecoevt_trig%d_%d_mult%d_%d_evtplane_sig", (int) ptmin, (int) ptmax, nmin, nmax), ";#Delta#phi", 48, -pi, 2*pi);
	h1genPhirecoevtplane_bkg = new TH1D(Form("h1_genphirecoevt_trig%d_%d_mult%d_%d_evtplane_bkg", (int) ptmin, (int) ptmax, nmin, nmax), ";#Delta#phi", 48, -pi, 2*pi);
	
	h1recoPhigenevtplane_sig = new TH1D(Form("h1_recophigenevt_trig%d_%d_mult%d_%d_evtplane_sig", (int) ptmin, (int) ptmax, nmin, nmax), ";#Delta#phi", 48, -pi, 2*pi);
	h1recoPhigenevtplane_bkg = new TH1D(Form("h1_recophigenevt_trig%d_%d_mult%d_%d_evtplane_bkg", (int) ptmin, (int) ptmax, nmin, nmax), ";#Delta#phi", 48, -pi, 2*pi);
	
	h1recoPhirecoevtplane_sig = new TH1D(Form("h1_recophirecoevt_trig%d_%d_mult%d_%d_evtplane_sig", (int) ptmin, (int) ptmax, nmin, nmax), ";#Delta#phi", 48, -pi, 2*pi);
	h1recoPhirecoevtplane_bkg = new TH1D(Form("h1_recophirecoevt_trig%d_%d_mult%d_%d_evtplane_bkg", (int) ptmin, (int) ptmax, nmin, nmax), ";#Delta#phi", 48, -pi, 2*pi);
	
	
	
	h1gentrkgenevtplane_sig = new TH1D(Form("h1_gentrkgenevt_trig%d_%d_mult%d_%d_evtplane_sig", (int) ptmin, (int) ptmax, nmin, nmax), ";#Delta#phi", 48, -pi, 2*pi);
	h1gentrkgenevtplane_bkg = new TH1D(Form("h1_gentrkgenevt_trig%d_%d_mult%d_%d_evtplane_bkg", (int) ptmin, (int) ptmax, nmin, nmax), ";#Delta#phi", 48, -pi, 2*pi);
	
	h1gentrkrecoevtplane_sig = new TH1D(Form("h1_gentrkrecoevt_trig%d_%d_mult%d_%d_evtplane_sig", (int) ptmin, (int) ptmax, nmin, nmax), ";#Delta#phi", 48, -pi, 2*pi);
	h1gentrkrecoevtplane_bkg = new TH1D(Form("h1_gentrkrecoevt_trig%d_%d_mult%d_%d_evtplane_bkg", (int) ptmin, (int) ptmax, nmin, nmax), ";#Delta#phi", 48, -pi, 2*pi);
	
	h1recotrkgenevtplane_sig = new TH1D(Form("h1_recotrkgenevt_trig%d_%d_mult%d_%d_evtplane_sig", (int) ptmin, (int) ptmax, nmin, nmax), ";#Delta#phi", 48, -pi, 2*pi);
	h1recotrkgenevtplane_bkg = new TH1D(Form("h1_recotrkgenevt_trig%d_%d_mult%d_%d_evtplane_bkg", (int) ptmin, (int) ptmax, nmin, nmax), ";#Delta#phi", 48, -pi, 2*pi);
	
	h1recotrkrecoevtplane_sig = new TH1D(Form("h1_recotrkrecoevt_trig%d_%d_mult%d_%d_evtplane_sig", (int) ptmin, (int) ptmax, nmin, nmax), ";#Delta#phi", 48, -pi, 2*pi);
	h1recotrkrecoevtplane_bkg = new TH1D(Form("h1_recotrkrecoevt_trig%d_%d_mult%d_%d_evtplane_bkg", (int) ptmin, (int) ptmax, nmin, nmax), ";#Delta#phi", 48, -pi, 2*pi);
	
	
	
	
	h1nevents->Fill(nentries);
	
	int nphis = 0;
	int ntrks = 0;
	int validevents=0;
	

	vector < vector <double> > Phicandeta;
	vector < vector <double> > Phicandphi;
	vector < vector <double> > Phicandm;
	vector < vector <double> > Phicandpt;
	vector < vector <double> > K1candindex;
	vector < vector <double> > K2candindex;
	
	vector < vector <double> > genPhieta;
	vector < vector <double> > genPhiphi;
	vector < vector <double> > genPhipt;
	
	vector < vector <double> > gentrketa;
	vector < vector <double> > gentrkphi;
	vector < vector <double> > gentrkpt;
	vector < vector <double> > gentrkid;
	vector < vector <double> > gentrkindex;
	
	vector < vector <double> > trketa;
	vector < vector <double> > trkphi;
	vector < vector <double> > trkpt;
	
	// vector <double> eventplaneangle;
	
	//Begin Event Loop
	for (Long64_t jentry = 0; jentry < nentries; jentry++) {
		// int nphis_inevents=0;
		if (Phicandeta.size() == 11) {
			Phicandeta.erase(Phicandeta.begin());
			Phicandphi.erase(Phicandphi.begin());
			Phicandm.erase(Phicandm.begin());
			Phicandpt.erase(Phicandpt.begin());
			K1candindex.erase(K1candindex.begin());
			K2candindex.erase(K2candindex.begin());
		}
		
		if (genPhiphi.size() == 11) {
			genPhieta.erase(genPhieta.begin());
			genPhiphi.erase(genPhiphi.begin());
			genPhipt.erase(genPhipt.begin());
		}
		
		if (gentrkphi.size() == 11) {
			gentrketa.erase(gentrketa.begin());
			gentrkphi.erase(gentrkphi.begin());
			gentrkpt.erase(gentrkpt.begin());
			gentrkid.erase(gentrkid.begin());
			gentrkindex.erase(gentrkindex.begin());
		}
		
		if (trkphi.size() == 11) {
			trketa.erase(trketa.begin());
			trkphi.erase(trkphi.begin());
			trkpt.erase(trkpt.begin());
		}
		
		
		vector <double> Phicandeta_evt;
		vector <double> Phicandphi_evt;
		vector <double> Phicandm_evt;
		vector <double> Phicandpt_evt;
		vector <double> K1candindex_evt;
		vector <double> K2candindex_evt;
		
		vector <double> genPhieta_evt;
		vector <double> genPhiphi_evt;
		vector <double> genPhipt_evt;
		
		vector <double> trketa_evt;
		vector <double> trkphi_evt;
		vector <double> trkpt_evt;
		
		vector <double> gentrketa_evt;
		vector <double> gentrkphi_evt;
		vector <double> gentrkpt_evt;
		vector <double> gentrkid_evt;
		vector <double> gentrkindex_evt;
		
		c->GetEntry(jentry);
		if (jentry%200==0 || jentry == 999) 		
		cout << "Entry " << jentry << "/" << nentries <<" Loaded (sig)" << endl;
		// if(skipevent(vzrange,999999)) continue;
		int thismult = 0;
		//Check Multiplicity
		for (int m = 0; m < c->track.nTrk; ++m) {
			if (Checktracks(c->track.trkEta[m], c->track.trkPt[m], c->track.trkPtError[m], c->track.trkDz1[m] , c->track.trkDzError1[m], c->track.trkDxy1[m], c->track.trkDxyError1[m], 3, c->track.highPurity[m]))
			{thismult++;}
		}
		

		if (thismult > 100) {		
		cout << "Mult(sig): " << thismult << endl;
		}
		if (thismult < nmin || thismult >= nmax)
		continue;
		
		double eventplanerescorrplus =1., eventplanerescorrminus=1.;
		if (thismult > 100)
			{
			eventplanerescorrplus = 0.143;
			eventplanerescorrminus = 0.237;
			}
		if (thismult > 120)
			{
			eventplanerescorrplus = 0.151;
			eventplanerescorrminus = 0.275;
			}
		if (thismult > 150)
			{
			eventplanerescorrplus = 0.162;
			eventplanerescorrminus = 0.321;
			}
		if (thismult > 185)
			{
			eventplanerescorrplus = 0.170;
			eventplanerescorrminus = 0.362;
			}
		if (thismult > 220)
			{
			eventplanerescorrplus = 0.177;
			eventplanerescorrminus = 0.402;
			}
		
		
		validevents++;
		// if (jentry%1000==0) {		
		// cout << "In multi class" << endl;}
		// int phicounter =0;
		//Make the Phis
		// if (genorreco == 2) {
		for (int i = 0; i < c->track.nTrk; ++i) {
			
			if (!Checktracks(c->track.trkEta[i], c->track.trkPt[i], c->track.trkPtError[i], c->track.trkDz1[i] , c->track.trkDzError1[i], c->track.trkDxy1[i], c->track.trkDxyError1[i], c->track.trkNHit[i], c->track.highPurity[i]))
			continue;
			
			if (c->track.trkPt[i] < 3.0) {
				trketa_evt.push_back(c->track.trkEta[i]);
				trkphi_evt.push_back(c->track.trkPhi[i]);
				trkpt_evt.push_back(c->track.trkPt[i]);
				ntrks++;

			}
			// if (genorreco == 0) 
			// if ((int)c->track.trkGMPId[i] != 333 && TMath::Abs((int)c->track.trkPId[i]) == 321) continue;
			
			for (int j = 0; j < c->track.nTrk; j++){
				if (!Checktracks(c->track.trkEta[j], c->track.trkPt[j], c->track.trkPtError[j], c->track.trkDz1[j] , c->track.trkDzError1[j], c->track.trkDxy1[j], c->track.trkDxyError1[j], c->track.trkNHit[j], c->track.highPurity[j]))
				continue;
				// if (genorreco == 0) {
				// if ((int)c->track.trkGMPId[i] !=333) continue;	
				// if ((int)c->track.trkPId[i] != -1*(int)c->track.trkPId[j]) continue;	
				// }
				if (c->track.trkCharge[i] != (-1) * c->track.trkCharge[j]) 
				continue;	// K+ K- should be of opposite sign
				Kcand1.SetPtEtaPhiM(c->track.trkPt[i], c->track.trkEta[i], c->track.trkPhi[i], mass_K);
				Kcand2.SetPtEtaPhiM(c->track.trkPt[j], c->track.trkEta[j], c->track.trkPhi[j], mass_K);
				Phicand = Kcand1 + Kcand2;
				
				// nphis++;
				// cout << nphis << ":" <<Phicand.Pt() << ":" << ptmin << ":" << ptmax << endl;
				if ((Phicand.Pt() <= ptmin)) 
				continue; // Choose pT bin
				if ((Phicand.Pt() > ptmax)) 
				continue; // Choose pT bin
				if (Phicand.M() > 1.1) 
				continue; // Not interesting 
				
				
				if (Phicand.M() < (phimass_pdg + phiwidth_pdg) && Phicand.M() >= (phimass_pdg - phiwidth_pdg)) {
					h3Phiptetaphi->Fill(Phicand.Pt(), Phicand.Eta(), Phicand.Phi());
				}
				h1Phimass->Fill(Phicand.M());
				Phicandeta_evt.push_back(Phicand.Eta());
				Phicandphi_evt.push_back(Phicand.Phi());
				Phicandm_evt.push_back(Phicand.M());
				Phicandpt_evt.push_back(Phicand.Pt());
				K1candindex_evt.push_back(i);
				K2candindex_evt.push_back(j);
				nphis++;
			}
			
		}
		Phicandeta.push_back(Phicandeta_evt);
		Phicandphi.push_back(Phicandphi_evt);
		Phicandm.push_back(Phicandm_evt);
		Phicandpt.push_back(Phicandpt_evt);
		K1candindex.push_back(K1candindex_evt);
		K2candindex.push_back(K2candindex_evt);
		trketa.push_back(trketa_evt);
		trkphi.push_back(trkphi_evt);
		trkpt.push_back(trkpt_evt);
		// if (jentry%1000==0) cout << nphis_inevents <<" Phis are made in this event" << endl;
		//Phis are made, now correlate
		
		// int phiarraysize = Phicandm.size();
		
		//Correlate phis, run over all the tracks in an event, then run over all the phis created. 
		if (validevents < 11) 
		continue;
		for (int mix = 0; mix < (int) Phicandm.size(); mix++) {
			for (int p = 0; p < (int) Phicandm[mix].size(); p++) {
				for (int k = 0; k < (int) trkphi_evt.size(); k++) {
					
					double deta = fabs(Phicandeta[mix][p]- trketa_evt[k]);
					double dphi = TMath::ACos(TMath::Cos(Phicandphi[mix][p]- trkphi_evt[k]));
					
					if (mix == (int) Phicandm.size() - 1)
					{
					       if (!(k == K1candindex[mix][p] || k == K2candindex[mix][p]))
						FillHist3D(h3PhiTrackSignal, Phicandm[mix][p], deta, dphi, 1.);		
					}
					else {FillHist3D(h3PhiTrackBackground, Phicandm[mix][p], deta, dphi, 1.);}
					
				}
			}
		}
		
		for (int mix = 0; mix < (int) trkphi.size(); mix++) {
			for (int p = 0; p < (int)trkphi[mix].size(); p++) {
				for (int k = 0; k < (int) trkphi_evt.size(); k++) {
					
					double deta = fabs(trketa[mix][p]- trketa_evt[k]);
					double dphi = TMath::ACos(TMath::Cos(trkphi[mix][p] - trkphi_evt[k]));
					if (mix == (int) trkphi.size() - 1)
					FillHist2D(h2TrackTrackSignal, deta, dphi, 1.);							
					else
					FillHist2D(h2TrackTrackBackground, deta, dphi, 1.);							
				}
			}
		}
		
		double deltaphigen,deltaphigen2, deltaetagen2 = 0;
		
		// if (genorreco == 0) {
		for (int i = 0; i < c->genparticle.mult; i++) {
			if (c->genparticle.eta[i] > 2.5) continue;
//COMMENTED OUT 
			if (c->genparticle.sta[i] == 1 && TMath::Abs(c->genparticle.chg[i]) != 0 && c->genparticle.pt[i] <= 3.0 && c->genparticle.pt[i] > 0.4) 
			{
				gentrketa_evt.push_back(c->genparticle.eta[i]);
				gentrkphi_evt.push_back(c->genparticle.phi[i]);
				gentrkpt_evt.push_back(c->genparticle.pt[i]);
				gentrkid_evt.push_back(c->genparticle.pdg[i]);
				gentrkindex_evt.push_back(i);
			}
			
			if (c->genparticle.pdg[i] == 333 && c->genparticle.pt[i] > ptmin && c->genparticle.pt[i] <= ptmax) 
			{
				genPhieta_evt.push_back(c->genparticle.eta[i]);
				genPhiphi_evt.push_back(c->genparticle.phi[i]);
				genPhipt_evt.push_back(c->genparticle.pt[i]);
			}
			
			// deltaphigen = TMath::ACos(TMath::Cos(c->genparticle.phi[i] - c->genparticle.phi0));
			// if (deltaphigen > pi ) deltaphigen = 2*pi - deltaphigen;
			// h1genPhicorr_evtplane->Fill(deltaphigen);
			// h1genPhicorr_evtplane->Fill(2*pi - deltaphigen);
			// h1genPhicorr_evtplane->Fill(-deltaphigen);
		}
		
		genPhipt.push_back(genPhipt_evt);
		genPhieta.push_back(genPhieta_evt);
		genPhiphi.push_back(genPhiphi_evt);
		
		gentrkpt.push_back(gentrkpt_evt);
		gentrketa.push_back(gentrketa_evt);
		gentrkphi.push_back(gentrkphi_evt);
		gentrkid.push_back(gentrkid_evt);
		gentrkindex.push_back(gentrkindex_evt);
		
		TLorentzVector genk1;
		TLorentzVector genk2;
		TLorentzVector genrecophi;	
		
		// double genphipt_temp, genphieta_temp, genphiphi_temp;
		double eventplaneangle = 0;
		double eventplanerescorr = 1.;
		// eventplanerescorr = sqrt(TMath::Cos(2*(c->evt.hiEvtPlanes[22]-c->evt.hiEvtPlanes[0]))*TMath::Cos(2*(c->evt.hiEvtPlanes[23]-c->evt.hiEvtPlanes[0]))/TMath::Cos(2*(c->evt.hiEvtPlanes[22]-c->evt.hiEvtPlanes[23])))
		// eventplanerescorr = sqrt(TMath::Cos(2*(c->evt.hiEvtPlanes[22] - c->evt.hiEvtPlanes[23])));
		
		for (int mix = 0; mix < (int) genPhiphi.size(); mix++) {
			for (int i = 0; i < (int) genPhiphi[mix].size(); i++) {			
    			  if (c->genparticle.phi0 == -10) continue;
			  // cout << "Evt Plane Angle: " << c->evt.genparticle.phi0 << endl;
				deltaphigen = TMath::ACos(TMath::Cos(genPhiphi[mix][i] - c->genparticle.phi0));
				if (mix == (int) genPhiphi.size() - 1) {
					h1genPhigenevtplane_sig->Fill(deltaphigen);
				}
				
				else 
				{
					h1genPhigenevtplane_bkg->Fill(deltaphigen);
				}
			}
		}		
		
		/////
		
		for (int mix = 0; mix < (int) Phicandm.size(); mix++) {
			for (int i = 0; i < (int) Phicandm[mix].size(); i++) {			
    			  if (c->genparticle.phi0 == -10) continue;
			  // cout << "Evt Plane Angle: " << c->evt.genparticle.phi0 << endl;
				deltaphigen = TMath::ACos(TMath::Cos(Phicandphi[mix][i] - c->genparticle.phi0));
				// if (deltaphigen > pi ) deltaphigen = 2*pi - deltaphigen;
				if (mix == (int) genPhiphi.size() - 1) {
					h1recoPhigenevtplane_sig->Fill(deltaphigen);

				}
				else 
				{
					h1recoPhigenevtplane_bkg->Fill(deltaphigen);
				}
			}
		}		
		
		////
		
		
		for (int mix = 0; mix < (int) genPhiphi.size(); mix++) {
			for (int i = 0; i < (int) genPhiphi[mix].size(); i++) {			
    			if (c->evt.hiEvtPlanes[21] == -10) continue;
			  // cout << "Evt Plane Angle: " << c->evt.genparticle.phi0 << endl;
				if (genPhieta[mix][i] < 0) {eventplaneangle = c->evt.hiEvtPlanes[22];eventplanerescorr = eventplanerescorrplus;}
				else {eventplaneangle = c->evt.hiEvtPlanes[23];eventplanerescorr = eventplanerescorrminus;}
			  // cout << "Evt Plane Angle: " << c->evt.genparticle.phi0 << endl;
				deltaphigen = TMath::ACos(TMath::Cos(genPhiphi[mix][i] - eventplaneangle));
				if (mix == (int) genPhiphi.size() - 1) {
					h1genPhirecoevtplane_sig->Fill(deltaphigen, 1./eventplanerescorr);
				}
				else 
				{
					h1genPhirecoevtplane_bkg->Fill(deltaphigen, 1./eventplanerescorr);
				}
			}
		}		
		
		/////
		
		for (int mix = 0; mix < (int) Phicandm.size(); mix++) {
			for (int i = 0; i < (int) Phicandm[mix].size(); i++) {			
    			if (c->evt.hiEvtPlanes[21] == -10) continue;
			  // cout << "Evt Plane Angle: " << c->evt.genparticle.phi0 << endl;
				if (Phicandeta[mix][i] < 0) {eventplaneangle = c->evt.hiEvtPlanes[22];eventplanerescorr = eventplanerescorrplus;}
				else {eventplaneangle = c->evt.hiEvtPlanes[23];eventplanerescorr = eventplanerescorrminus;}
				deltaphigen = TMath::ACos(TMath::Cos(Phicandphi[mix][i] - eventplaneangle));
				// if (deltaphigen > pi ) deltaphigen = 2*pi - deltaphigen;
				if (mix == (int) Phicandm.size() - 1) {
					h1recoPhirecoevtplane_sig->Fill(deltaphigen, 1./eventplanerescorr);
				}
				else 
				{
					h1recoPhirecoevtplane_bkg->Fill(deltaphigen, 1./eventplanerescorr);
				}
			}
		}		
		
		//////////
		
		for (int mix = 0; mix < (int) gentrkphi.size(); mix++) {
			for (int i = 0; i < (int) gentrkphi[mix].size(); i++) {			
			  if (c->genparticle.phi0 == -10) continue;  
				deltaphigen = TMath::ACos(TMath::Cos(gentrkphi[mix][i] - c->genparticle.phi0));
				if (mix == (int) gentrkphi.size() - 1) {
					h1gentrkgenevtplane_sig->Fill(deltaphigen);
				}
				else 
				{
					h1gentrkgenevtplane_bkg->Fill(deltaphigen);
				}
			}
		}		
		// cout << "Reach this point" << endl;
		
		//////////
		
		for (int mix = 0; mix < (int) gentrkphi.size(); mix++) {
			for (int i = 0; i < (int) gentrkphi[mix].size(); i++) {			
			  if (c->evt.hiEvtPlanes[21] == -10) continue;
			  // cout << "Evt Plane Angle: " << c->evt.genparticle.phi0 << endl;
				if (gentrketa[mix][i] < 0) {eventplaneangle = c->evt.hiEvtPlanes[22];eventplanerescorr = eventplanerescorrplus;}
				else {eventplaneangle = c->evt.hiEvtPlanes[23]; eventplanerescorr = eventplanerescorrminus;}
				deltaphigen = TMath::ACos(TMath::Cos(gentrkphi[mix][i] - eventplaneangle));
				// if (deltaphigen > pi ) deltaphigen = 2*pi - deltaphigen;
				if (mix == (int) gentrkphi.size() - 1) {
					h1gentrkrecoevtplane_sig->Fill(deltaphigen, 1./eventplanerescorr);
				}
				else 
				{
					h1gentrkrecoevtplane_bkg->Fill(deltaphigen, 1./eventplanerescorr);
				}
			}
		}		
		//////////
		
		for (int mix = 0; mix < (int) trkphi.size(); mix++) {
			for (int i = 0; i < (int) trkphi[mix].size(); i++) {			
				if (c->evt.hiEvtPlanes[21] == -10) continue;
				// cout << "Evt Plane Angle: " << c->evt.hiEvtPlanes[21] << endl;
				eventplaneangle = c->evt.hiEvtPlanes[21];
				if (trketa[mix][i] < 0) {eventplaneangle = c->evt.hiEvtPlanes[22];eventplanerescorr = eventplanerescorrplus;}
				else {eventplaneangle = c->evt.hiEvtPlanes[23];eventplanerescorr = eventplanerescorrminus;}
				deltaphigen = TMath::ACos(TMath::Cos(trkphi[mix][i] - eventplaneangle));
				
				if (mix == (int) trkphi.size() - 1) {
					h1recotrkrecoevtplane_sig->Fill(deltaphigen, 1./eventplanerescorr);
					}
				else 
				{
					h1recotrkrecoevtplane_bkg->Fill(deltaphigen, 1./eventplanerescorr);
				}
			}
		}		
			
		//////////
		
		for (int mix = 0; mix < (int) trkphi.size(); mix++) {
			for (int i = 0; i < (int) trkphi[mix].size(); i++) {			
			  if (c->genparticle.phi0 == -10) continue;  
				deltaphigen = TMath::ACos(TMath::Cos(trkphi[mix][i] - c->genparticle.phi0));
				// if (deltaphigen > pi ) deltaphigen = 2*pi - deltaphigen;
				if (mix == (int) gentrkphi.size() - 1) {
					h1recotrkgenevtplane_sig->Fill(deltaphigen);
				}
				else 
				{
					h1recotrkgenevtplane_bkg->Fill(deltaphigen);
				}
			}
		}		
		
	
		
		for (int i = 0; i < (int) genPhiphi_evt.size(); i++) {
			h3Phiptetaphi_gen->Fill(genPhipt_evt[i], genPhieta_evt[i], genPhiphi_evt[i]);
		}
		
		int skipphi = 0;
		if (validevents < 11) 
		continue;
		for (int mix = 0; mix < (int) gentrkphi.size(); mix++) {
			for (int i = 0; i < (int) gentrkphi[mix].size(); i++) {			
				for (int j = 0; j < (int) genPhiphi_evt.size(); j++) {
					skipphi = 0;
					//if (TMath::Abs(gentrkid[mix][i]) == 321 && (mix == (int) gentrkphi.size() - 1)){
					  //for (int k = 0; k < (int) gentrkphi[mix].size(); k++) {
							// if (c->genparticle.chg[j] == c->genparticle.chg[k]) continue;
							// if (TMath::Abs(c->genparticle.eta[k]) > 2.5) continue;
							//if (TMath::Abs(gentrkid[mix][k]) != 321) continue;	
							//genk1.SetPtEtaPhiM(gentrkpt[mix][i],gentrketa[mix][i],gentrkphi[mix][i], mass_K);
							//genk2.SetPtEtaPhiM(gentrkpt[mix][k],gentrketa[mix][k],gentrkphi[mix][k], mass_K);
							//genrecophi = genk1 +genk2;
							
							//if (genPhiphi_evt[j] - genrecophi.Phi() < 0.1 && genPhieta_evt[j] - genrecophi.Eta() < 0.1) 
							//{skipphi = 1;	
							//	break;
							//}
					  //}
					  //}
					deltaetagen2 = genPhieta_evt[j] - gentrketa[mix][i];
					deltaphigen2 = TMath::ACos(TMath::Cos(genPhiphi_evt[j] - gentrkphi[mix][i]));
					if (mix == (int) gentrkphi.size() - 1 && skipphi == 0) {
						FillHist2D(h2genPhisig_twopart, deltaetagen2, deltaphigen2, 1.);
					}
					else if (mix !=(int) gentrkphi.size() -1)
					        FillHist2D(h2genPhibkg_twopart, deltaetagen2, deltaphigen2, 1.);
					
				}
				
				for (int j = 0; j < (int) gentrkphi_evt.size(); j++) {
					deltaetagen2 = gentrketa_evt[j] - gentrketa[mix][i];
					deltaphigen2 = TMath::ACos(TMath::Cos(gentrkphi_evt[j] - gentrkphi[mix][i]));
					if (mix == (int) gentrkphi.size() - 1) {
						FillHist2D(h2gentrksig_twopart, deltaetagen2, deltaphigen2, 1.);
					}
					else if (mix !=(int) gentrkphi.size() - 1)
					        FillHist2D(h2gentrkbkg_twopart, deltaetagen2, deltaphigen2, 1.);
					
				}				
			}
		}		
	}
	h1genPhigenevtplane_sig->Sumw2();
	h1genPhigenevtplane_bkg->Sumw2();
	
	h1genPhirecoevtplane_sig->Sumw2();
	h1genPhirecoevtplane_bkg->Sumw2();
	
	h1recoPhigenevtplane_sig->Sumw2();
	h1recoPhigenevtplane_bkg->Sumw2();
	
	h1recoPhirecoevtplane_sig->Sumw2();
	h1recoPhirecoevtplane_bkg->Sumw2();
	
	h1gentrkgenevtplane_sig->Sumw2();
	h1gentrkgenevtplane_bkg->Sumw2();
	
	h1gentrkrecoevtplane_sig->Sumw2();
	h1gentrkrecoevtplane_bkg->Sumw2();
	
	h1recotrkgenevtplane_sig->Sumw2();
	h1recotrkgenevtplane_bkg->Sumw2();
	
	h1recotrkrecoevtplane_sig->Sumw2();
	h1recotrkrecoevtplane_bkg->Sumw2();
	

	h2genPhisig_twopart->Sumw2();
	h2genPhibkg_twopart->Sumw2();
	
	h1gentrkgenevtplane_sig->Sumw2();
	h1gentrkgenevtplane_bkg->Sumw2();

	h2gentrksig_twopart->Sumw2();
	h2gentrkbkg_twopart->Sumw2();

	h1Phimass->Sumw2();
	
	h3PhiTrackSignal->Sumw2();
	h3PhiTrackBackground->Sumw2();
	
	h2TrackTrackSignal->Sumw2();
	h2TrackTrackBackground->Sumw2();
	
	hnphis->SetBinContent(1, nphis);
	hntrks->SetBinContent(1, ntrks);
	hntrks->Sumw2();
	hntrks->Sumw2();
	return h3PhiTrackSignal;
}

// TH3D *hPhiCorrBackground(int nmin, int nmax, double ptmin, double ptmax)
// {

// Long64_t nentries = c->GetEntries();

// TLorentzVector Kcand1, Kcand2, Phicand;
// // Long64_t nentries = 1000;

// TH3D *h3PhiTrackBackground = new TH3D(Form("h3bkg_mdetadphi_phitrk_trig%d_%d_mult%d_%d", (int) ptmin, (int) ptmax, nmin, nmax),  ";M_{KK};#Delta#phi;#Delta#phi", 240, 0.98, 1.1, 40, -5, 5, 64, -2*pi, 2 * pi);

// // Begin Event Loop
// for (Long64_t jentry = 0; jentry < nentries; jentry++) {

// vector <double> Phicandeta;
// vector <double> Phicandphi;
// vector <double> Phicandm;
// vector <double> Phicandpt;
// vector <double> K1candindex;
// vector <double> K2candindex;

// c->GetEntry(jentry);

// // if(skipevent(vzrange,999999)) continue;
// int thismult = 0;
// // Check Multiplicity
// for (int m = 0; m < c->track.nTrk; ++m) {
// if (Checktracks(c->track.trkEta[m], c->track.trkPt[m], c->track.trkPtError[m], c->track.trkDz1[m] , c->track.trkDzError1[m], c->track.trkDxy1[m], c->track.trkDxyError1[m], 3, c->track.highPurity[m]))
// thismult++;
// }
// // if (jentry%1000==0) {		
// // cout << "Mult(bkg): " << thismult << endl;}
// if (thismult < nmin || thismult >= nmax)
// continue;

// // Make the Phis
// for (int i = 0; i < c->track.nTrk; ++i) {

// if (!Checktracks(c->track.trkEta[i], c->track.trkPt[i], c->track.trkPtError[i], c->track.trkDz1[i] , c->track.trkDzError1[i], c->track.trkDxy1[i], c->track.trkDxyError1[i], c->track.trkNHit[i], c->track.highPurity[i]))
// continue;

// for (int j = 0; j < c->track.nTrk; j++){
// if (!Checktracks(c->track.trkEta[j], c->track.trkPt[j], c->track.trkPtError[j], c->track.trkDz1[j] , c->track.trkDzError1[j], c->track.trkDxy1[j], c->track.trkDxyError1[j], c->track.trkNHit[j], c->track.highPurity[j]))
// continue;
// if (c->track.trkCharge[i] != (-1) * c->track.trkCharge[j]) 
// continue;	// K+ K- should be of opposite sign
// Kcand1.SetPtEtaPhiM(c->track.trkPt[i], c->track.trkEta[i], c->track.trkPhi[i], mass_K);
// Kcand2.SetPtEtaPhiM(c->track.trkPt[j], c->track.trkEta[j], c->track.trkPhi[j], mass_K);
// Phicand = Kcand1 + Kcand2;

// if (Phicand.Pt() < ptmin || Phicand.Pt() > ptmax) 
// continue; // Choose pT bin
// if (Phicand.M() > 1.1) continue; // Not interesting 

// // h1Phimass->Fill(Phicand.M());
// // if (Phicand.M() < phimass_pdg + phiwidth_pdg && Phicand.M > phimass_pdg - phiwidth_pdg) {
// // h3Phiptetaphi->Fill(Phicand.Pt(), Phicand.Eta(), Phicand.Phi());
// // }
// // h1Phimass->Fill(Phicand.M());
// Phicandeta.push_back(Phicand.Eta());
// Phicandphi.push_back(Phicand.Phi());
// Phicandm.push_back(Phicand.M());
// Phicandpt.push_back(Phicand.Pt());
// K1candindex.push_back(i);
// K2candindex.push_back(j);

// }

// }

// // Phis are made, now correlate

// // int phiarraysize = Phicandm.size();

// // Correlate phis, run over all the tracks in an event, then run over all the phis created. 
// for (int p = 0; p < 500; p++) {
// for (int mix = 1; mix < 11; mix++) {
// if (jentry < 10) continue;
// c->GetEntry(jentry - mix);
// for (int k = 0; k < c->track.nTrk; k++) {

// if (k == K1candindex[p] || k == K2candindex[p])
// continue;
// if (!Checktracks(c->track.trkEta[k], c->track.trkPt[k], c->track.trkPtError[k], c->track.trkDz1[k] , c->track.trkDzError1[k], c->track.trkDxy1[k], c->track.trkDxyError1[k], 3, c->track.highPurity[k]))
// continue;
// if (c->track.trkPt[k] > 3.0)
// continue;

// double deta = fabs(Phicandeta[p]- c->track.trkEta[k]);
// double dphi = fabs(Phicandphi[p]- c->track.trkPhi[k]);

// FillHist3D(h3PhiTrackBackground, Phicandm[p], deta, dphi, 1.);		
// }
// }
// }
// }
// return h3PhiTrackBackground;
// }


bool Checktracks(double tracketa, double trackpt, double trackpterror, double dz, double dzerror, double dxy, double dxyerror, int nhits, bool highpurity)
{	
	if (fabs(tracketa) < 2.4 &&trackpt > 0.4 && fabs(dz / dzerror) < 3 && fabs(dxy / dxyerror) < 3 && trackpterror / trackpt < 0.1&& nhits >= 3 && highpurity==1)
	return 1;
	else 
	return 0;
}

int GetNTotTrig()
{
	int tmp = ntottrig;
	ntottrig = 0;
	return tmp;
}

void FillHist3D(TH3D *hist, double Phicandm, double deltaeta, double deltaphi, double weight)
{
	//m pt phi or pt eta phi
	if (weight < 0.1 ) return;
	// if (deltaphi > pi ) deltaphi = 2*pi - deltaphi;
	hist->Fill(Phicandm, deltaeta, deltaphi, 1./weight);
	hist->Fill(Phicandm, -deltaeta, deltaphi, 1./weight);
	hist->Fill(Phicandm, deltaeta, -deltaphi, 1./weight);
	hist->Fill(Phicandm, -deltaeta, -deltaphi, 1./weight);
	hist->Fill(Phicandm, deltaeta, (2 * pi) - deltaphi, 1./weight);
	hist->Fill(Phicandm, -deltaeta, (2 * pi) - deltaphi, 1./weight);
}

void FillHist2D(TH2D *hist, double deltaeta, double deltaphi, double weight)
{
	if (weight < 0.0001 ) return;
	// if (deltaphi > pi ) deltaphi = 2*pi - deltaphi;
	hist->Fill(deltaeta, deltaphi, 1./weight);
	hist->Fill(-deltaeta, deltaphi, 1./weight);
	hist->Fill(deltaeta, -deltaphi, 1./weight);
	hist->Fill(-deltaeta, -deltaphi, 1./weight);
	hist->Fill(deltaeta, (2 * pi) - deltaphi, 1./weight);
	hist->Fill(-deltaeta, (2 * pi) - deltaphi, 1./weight);
}

