#include "utilities.h"
#include "plotFigure.C"
double mcsigma[10] = {0,0.000949111,0.00148039,0.00147905,0.00183143,0.00155642,0.00135161,0.0018135,0.00253935,0.00172255};
const int nBins = 9;
double bins[nBins+1] = {1.5, 2., 2.5, 3., 4.0, 5., 6., 7.4, 10.4, 19.9};

TF1 *fit(TTree *nt,double ptMin,double ptMax, int mode)
{   
   static int count=0;
   count++;
   //TCanvas *c= new TCanvas(Form("c%d",count),"",600,600);
   //Main histogram
   TH1D *h = new TH1D(Form("h%d%d",count, mode),"",300,0.8995,1.1995);
   //Fit histogram  
   TF1 *f = new TF1(Form("f%d%d",count, mode),"[0]*TMath::Voigt(x-[1],[2],[3])+[4]+[5]*x+[6]*x*x");
   //Set parameters
//double mcsigma[10] = {0,0.000949111,0.00148039,0.00147905,0.00183143,0.00155642,0.00135161,0.0018135,0.00253935,0.00172255};
//const int nBins = 9;
//double bins[nBins+1] = {1.5, 2., 2.5, 3., 4.0, 5., 6., 7.4, 10.4, 19.9};

   int binnum; 
   for (int i = 0; i < nBins+1; i++) {
    if (ptMax == bins[i+1])
	binnum = i;
}

   f->SetParameters(1,1.019,mcsigma[binnum],0.004, 1, -1, 1);
   f->FixParameter(1,1.019);
   f->FixParameter(2,mcsigma[binnum]);
   f->FixParameter(3,4e-3);
   f->SetRange(.99,1.05);
   //Plot Phis
   
   if (mode == 0)   nt->Draw(Form("phiM>>h%d%d",count, mode),Form("(phiPt<%f&&phiPt>=%f)", ptMax, ptMin));
   //else if (mode ==0) nt->Draw(Form("phiM>>h%d%d",count, mode),Form("effweight*(phiPt<%f&&phiPt>=%f &&((TMath::Abs(trkPId1) < 322)&&(TMath::Abs(trkPId1) > 320))&&((TMath::Abs(trkPId2) < 322)&&(TMath::Abs(trkPId2) > 320))&&((trkGMPId1 > 334 || trkGMPId1 < 332) && (trkGMPId2 > 334 || trkGMPId2 < 332)))", ptMax, ptMin));
   //else if (mode ==1) nt->Draw(Form("phiM>>h%d%d",count, mode),Form("effweight*(phiPt<%f&&phiPt>=%f &&((TMath::Abs(trkPId1) < 322)&&(TMath::Abs(trkPId1) > 320))&&((TMath::Abs(trkPId2) < 322)&&(TMath::Abs(trkPId2) > 320))&&(((trkGMPId1 > 334 || trkGMPId1 < 332) && (trkGMPId2 < 334 && trkGMPId2 > 332)) || ((trkGMPId2 > 334 || trkGMPId2 < 332) && (trkGMPId1 < 334 && trkGMPId1 > 332))))", ptMax, ptMin));
   else if (mode ==2) nt->Draw(Form("phiM>>h%d%d",count, mode),Form("(phiPt<%f&&phiPt>=%f && (trkGMPId1 == 333 &&trkGMPId2 == 333))", ptMax, ptMin));
   //Fit histogram
   h->Fit(Form("f%d%d",count, mode),"B","",1,1.04);
//   h->Fit(Form("f%d%d",count, mode),"LL","",1,1.04);
   f->ReleaseParameter(1);
//   f->ReleaseParameter(2);
   f->ReleaseParameter(3);
  // h->Fit(Form("f%d%d",count, mode),"LL","",1.,1.04);
   h->Fit(Form("f%d%d",count, mode),"R","",.99,1.05);
//   h->Fit(Form("f%d%d",count, mode),"RM","",1,1.04);
   h->SetMarkerSize(0.8);
   h->SetMarkerStyle(20);

   //Background from Brett Wigner method
   TF1 *background = new TF1("background","[0]+[1]*x+[2]*x*x");
   background->SetParameter(0,f->GetParameter(4));
   background->SetParameter(1,f->GetParameter(5));
   background->SetParameter(2,f->GetParameter(6));
//  background->SetParameter(3,f->GetParameter(7));
//   background->SetParameter(4,f->GetParameter(8));
   background->SetLineColor(4);
   background->SetRange(0.99,1.05);
   background->SetLineStyle(2);
   TF1 *mass = new TF1("mass","[0]*TMath::Voigt(x-[1],[2],[3])");
   mass->SetParameters(f->GetParameter(0),f->GetParameter(1),f->GetParameter(2), f->GetParameter(3));
   mass->SetParError(0,f->GetParError(0));
   mass->SetParError(1,f->GetParError(1));
   mass->SetParError(2,f->GetParError(2));
   mass->SetParError(3,f->GetParError(3));
   mass->SetRange(.99,1.05);
   mass->SetLineColor(2);
   mass->SetLineStyle(2);
  // cout <<mass->Integral(0,1.2)<<" "<<mass->IntegralError(0,1.2)<<endl;
   h->SetMarkerStyle(24);
   h->SetStats(0);
   h->Draw("e");
   h->SetXTitle("M_{KK} (GeV/c^{2})");
   h->SetYTitle("Entries / (1 MeV/c^{2})");
   h->GetXaxis()->CenterTitle();
   h->GetYaxis()->CenterTitle();
   h->SetTitleOffset(1.65,"Y");
   h->SetAxisRange(0.985,1.07,"X");
   h->SetAxisRange(0,h->GetMaximum()*1.2,"Y");
   background->Draw("same");   
   mass->Draw("same");
   mass->SetLineStyle(2);
   mass->SetFillStyle(3004);
   mass->SetFillColor(2);

   f->Draw("same"); 
   

   //leg2->AddEntry(h,"#phi meson p_{T}> 4 GeV/c","");
TLegend *leg2 = myLegend(0.45,0.43,0.9,0.76);
   leg2->AddEntry(h,"Voigt+2poly, fixed #sigma","");
   leg2->AddEntry(h,Form("%.1f Gev/c -%.1f GeV/c",ptMin,ptMax),"");
//   leg2->AddEntry(h,Form("M_{#phi}=%.2f #pm %.2f GeV/c^{2}",f->GetParameter(1),f->GetParError(1)),"");
   leg2->AddEntry(h,Form("#Gamma_{#phi}=%.3f #pm %.3f",f->GetParameter(3)*1000.,f->GetParError(3)*1000.),"");
   leg2->AddEntry(h, Form("#Chi^{2}/dof = %f", f->GetChisquare()/f->GetNDF()), "");
   leg2->SetTextSize(0.04);
   leg2->Draw();
    if (ptMin ==1.9 || ptMin ==3.4 || ptMin ==5.9){  
   TLegend *leg = myLegend(0.150,0.72,0.66,0.92);
   leg->AddEntry(h,"CMS Preliminary","");
   leg->AddEntry(h,"MC Hijing","");
  // leg->AddEntry(h,"p+Pb #sqrt{s_{NN}}= 5.02 TeV","pl"); 
   leg->SetTextSize(0.04);
   leg->Draw("same");
   }
  // c->Write();
   //c->SaveAs(Form("phiFigure/phiMass-%d.C",count));
   //c->SaveAs(Form("phiFigure/phiMass-%d.gif",count));
   //c->SaveAs(Form("phiFigure/phiMass-%d.eps",count));
   return f;	
}

/*
void mixedbackground(TTree *nt, TTree *bckgr, float ptMin, float ptMax){

//TFile *inf = new TFile("/net/hisrv0001/home/zhukova/CMSSW_5_3_8_HI/src/macro/temp/outputPhi_opppairs.root");
//TTree *bckgr = (TTree*) inf->Get("bckgr");
//TTree *nt = (TTree*) inf->Get("nt");

TH1D *hb = new TH1D ("hb", "hb", 100, 0.99, 1.06);
TH1D *hs = new TH1D ("hs", "hs", 100, 0.99, 1.06);
TH1D *hmass = new TH1D("hmass", "hmass", 100, 0.99, 1.06);
//TH1D *hfinal = new TH1D ("hfinal", "hfinal", 100, 1, 1.06);

bckgr->Draw("phiM >> hb", Form("pt > %lf && pt < %lf", ptMin, ptMax));
nt->Draw("phiM >> hs",Form("pt > %lf && pt < %lf", ptMin, ptMax) , "same");
int binmin = hb->FindBin(1.0);
int binmax = hb->FindBin(1.05);
float bmin = hb->GetBinContent(binmin);
float smin = hs->GetBinContent(binmin);
float bmax = hb->GetBinContent(binmax);
float smax = hs->GetBinContent(binmax);
float scaling = (smax+smin)/(bmax+bmin);
hb->Scale(scaling);
hmass = hs;
hmass->Add(hb, -1);
hmass->Draw();
}


void plotbg(TTree *bckgr, int index, float ptMin, float ptMax)
{
TH1D *hb = new TH1D(Form("hb%d",index),"",300,0.985,1.07);
bckgr->Draw(Form("phiM>>hb%d", index), Form("pt<%f&&pt>=%f", ptMax, ptMin));
hb->SetLineColor(4);
//hb->SetRange(0.995,1.06);
hb->SetLineStyle(2);
TLegend *leg = myLegend(0.450,0.72,0.90,0.92);
leg->AddEntry(hb,"CMS Preliminary","");
leg->AddEntry(hb,"p+Pb #sqrt{s_{NN}}= 5.02 TeV","pl");
leg->AddEntry(hb,Form("%f Gev/c -%f GeV/c",ptMin,ptMax),"");
leg->SetTextSize(0.04);
leg->Draw();

}
*/

void fitPhi_all2(char *infname)
{
   //Make Canvas
   TCanvas *call1 = new TCanvas("call1","All hadron pairs (1)",900,400);
   makeMultiPanelCanvas(call1,3,1,0.0,0.0,0.2,0.2,0.02);
   TCanvas *call2 = new TCanvas("call2","All hadron pairs (2)",900,400);
   makeMultiPanelCanvas(call2,3,1,0.0,0.0,0.2,0.2,0.02);
   TCanvas *call3 = new TCanvas("call3","All hadron pairs (3)",900,400);
   makeMultiPanelCanvas(call3,3,1,0.0,0.0,0.2,0.2,0.02);
   
  // TCanvas *cone = new TCanvas("cone","One true daughters",900,700);
   //makeMultiPanelCanvas(cone,3,2,0.0,0.0,0.2,0.2,0.02);
   TCanvas *cboth1 = new TCanvas("cboth1","Matched Pairs (1) ",900,400);
   makeMultiPanelCanvas(cboth1,3,1,0.0,0.0,0.2,0.2,0.02);
   TCanvas *cboth2 = new TCanvas("cboth2","Matched Pairs (2) ",900,400);
   makeMultiPanelCanvas(cboth2,3,1,0.0,0.0,0.2,0.2,0.02);
   TCanvas *cboth3 = new TCanvas("cboth3","Matched Pairs (3) ",900,400);
   makeMultiPanelCanvas(cboth3,3,1,0.0,0.0,0.2,0.2,0.02);
   //TCanvas *cnone = new TCanvas("cnone","No true daughters",900,700);
   //makeMultiPanelCanvas(cnone,3,2,0.0,0.0,0.2,0.2,0.02);
 //  TCanvas *cbckg = new TCanvas("cbckg","",900,700);
  // makeMultiPanelCanvas(cbckg,3,2,0.0,0.0,0.2,0.2,0.02);
   //Mixed Event Canvas
   
 //  TCanvas *cmix = new TCanvas("cmix","",900,700);
  // makeMultiPanelCanvas(cmix,3,2,0.0,0.0,0.2,0.2,0.02);
   
   //Define input file and ntuple
   TFile *inf = new TFile(infname);
   TTree *nt = (TTree*) inf->Get("nt_all");
   //int nEv;
   //nt->SetBranchAddress("nEv", &nEv);
   //nt->GetEntry(0);
   //int nentries = nEv;
   cout << "Check 1";
  // TTree *bckgr = (TTree*) inf->Get("bckgr");
  // cout << "Check 1";
 /*  TH1D *hb1 = new TH1D("hb1","",300,0.8995,1.1995);
   TH1D *hb2 = new TH1D("hb2","",300,0.8995,1.1995);
   TH1D *hb3 = new TH1D("hb3","",300,0.8995,1.1995);
   TH1D *hb4 = new TH1D("hb4","",300,0.8995,1.1995);
   TH1D *hb5 = new TH1D("hb5","",300,0.8995,1.1995);
   TH1D *hb6 = new TH1D("hb6","",300,0.8995,1.1995);
*/
   TFile *outf = new TFile("phiHistos_MC.root","recreate");
//////////////////////////////
//CHANGE NUMBER OF BINS HERE//
//////////////////////////////   
//   const int nBins = 9;
/////////////////////
//CHANGE BIN LIMITS//
/////////////////////

   
   //Define histogram
  // TH1D *hYield = new TH1D("hYield","",nBins,bins);
   TH1D *hMass = new TH1D("hMass","",nBins,bins);
   TH1D *hMult = new TH1D("hMult","",500,0,500);
   
    TH1D *hYielda = new TH1D("hYielda","",nBins,bins);
    TH1D *hYield2 = new TH1D("hYield2","",nBins,bins);
	//TH1D *hYielda3 = new TH1D("hYielda3","",nBins,bins);
	//TH1D *hYield1 = new TH1D("hYield1","",nBins,bins);
/*	TH1D *hYieldb1 = new TH1D("hYieldb1","",nBins,bins);
	TH1D *hYieldb2 = new TH1D("hYieldb2","",nBins,bins);
	TH1D *hYieldb3 = new TH1D("hYieldb3","",nBins,bins);*/
	//TH1D *hYield0 = new TH1D("hYield0","",nBins,bins);
   
   //Spectra
   float phimass[9]={0};
   float phimasserror[9]={0};

   for (int i=1;i<=nBins;i++)
   {   
//     ntEvt->Project("hMult","N",Form("N>=%.0f&&N<%.0f",bins[i-1],bins[i]));
	
     if (i <= 3){ 	 call1->cd(i);	 }
	 else if (i >3 && i <7){	 call2->cd(i-3);}
	 else if (i >6 && i <10){	 call3->cd(i-6);	 }
	 
	 TF1 *f = fit(nt,bins[i-1],bins[i], 0);
	 
	 if (i <= 3){ 	 cboth1->cd(i);	 }
	 else if (i >3 && i <7){	 cboth2->cd(i-3);	 }
	 else if (i >6 && i <10){	 cboth3->cd(i-6);	 }
	 
	 
	 TF1 *f2 = fit(nt,bins[i-1],bins[i], 2);
	 hYielda->SetBinContent(i,f->GetParameter(0)*1000.);
     hYielda->SetBinError(i,f->GetParError(0)*1000.);
	 	 
	 hYield2->SetBinContent(i,f2->GetParameter(0)*1000.);
     hYield2->SetBinError(i,f2->GetParError(0)*1000.);
	 
     hMass->SetBinContent(i,f->GetParameter(1)*1.);
     hMass->SetBinError(i,f->GetParError(1)*1.);
	 
	/* cnone->cd(i);
	 TF1 *f0 = fit(nt,bins[i-1],bins[i], 0);
	 hYield0->SetBinContent(i,f0->GetParameter(0)*1000.);
     hYield0->SetBinError(i,f0->GetParError(0)*1000.);
	 
	 cone->cd(i);
	 TF1 *f1 = fit(nt,bins[i-1],bins[i], 1);
	 hYield1->SetBinContent(i,f1->GetParameter(0)*1000.);
     hYield1->SetBinError(i,f1->GetParError(0)*1000.);	
	 cboth1->cd(i);
	 TF1 *fb1 = fit(nt,bins[i-1],bins[i], 2);
	 hYield2->SetBinContent(i,f2->GetParameter(0)*1000.);
     hYield2->SetBinError(i,f2->GetParError(0)*1000.);
*/	
	// cbckg->cd(i);
	// plotbg(bckgr, i,bins[i-1],bins[i]);
	// 
   }  

 /*  TCanvas *cSpectra = new TCanvas("cSpectra","Spectra",600,600,600,600);
   hYielda->SetXTitle("p_{T} (in GeV)");
   hYielda->SetYTitle("N_{#phi}");
   hYielda->GetXaxis()->CenterTitle();
   hYielda->GetYaxis()->CenterTitle();
   hYielda->SetTitleOffset(1.2,"Y");
   gPad->SetLogy();
   hYielda->Draw(); 
   //cSummary->SaveAs("phiFigure/phiYieldFraction.C");
  // cSpectra->SaveAs("phiYieldFraction.gif");
   //cSummary->SaveAs("phiFigure/phiYieldFraction.eps");*/
   
  TCanvas *cSpectraa = new TCanvas("cSpectraa","Spectra Compared",600,600,600,600);
   hYielda->SetXTitle("p_{T} (in GeV)");
   hYielda->SetYTitle("N_{#phi}");
   hYielda->GetXaxis()->CenterTitle();
   hYielda->GetYaxis()->CenterTitle();
   hYielda->SetTitleOffset(1.2,"Y");
   gPad->SetLogy();
   hYielda->SetMarkerColor(4);
   hYielda->SetMarkerStyle(24);
   hYielda->Draw(); 
     
   hYield2->SetXTitle("p_{T} (in GeV)");
   hYield2->SetYTitle("N_{#phi}");
   hYield2->GetXaxis()->CenterTitle();
   hYield2->GetYaxis()->CenterTitle();
   hYield2->SetTitleOffset(1.2,"Y");
   gPad->SetLogy();
   hYield2->SetMarkerColor(6);
   hYield2->SetMarkerStyle(24);
   hYield2->Draw("psame"); 
   
   TH1 *h;
   TLegend *leg2 = myLegend(0.65,0.65,0.9,0.75);
   TLegend *leg = myLegend(0.65,0.75,0.9,0.9);
   leg->AddEntry(h,"CMS Preliminary","");
   leg->AddEntry(h,"p+Pb #sqrt{s_{NN}}= 5.02 TeV","");
   leg->AddEntry(h,"MC HIJING (1.8M events)","");
   
   //leg2->AddEntry(h,"#phi meson p_{T}> 4 GeV/c","");
   leg->SetTextSize(0.02);
   leg->Draw();
   leg2->AddEntry(hYielda,"All hadron pairs","p");
   leg2->AddEntry(hYield2,"Matched Kaon pairs","p");
   leg2->SetTextSize(0.02);
   leg2->Draw(); 
   
  TCanvas *cRatio = new TCanvas("cRatio","Ratio",600,600,600,600);
   cRatio->cd();
   TH1F *hRatio = (TH1F*)hYielda->Clone();
   hRatio->SetName("hRatio");
   hRatio->Divide(hYield2);
   hRatio->SetXTitle("p_{T} (in GeV)");
   hRatio->SetYTitle("N_{#phi_{reco}}/N_{#phi_{true}}");
   hRatio->GetXaxis()->CenterTitle();
   hRatio->GetYaxis()->CenterTitle();
   hRatio->SetTitleOffset(1.2,"Y");
   hRatio->SetAxisRange(0,3,"Y");

   TLine *l = new TLine(2,1,20,1);
   l->SetLineStyle(2);
   TLegend *leg3 = myLegend(0.55,0.72,0.96,0.82);
   leg3->AddEntry(hRatio,"MC HIJING (1.8M events)","");
   leg3->AddEntry(h,"CMS Preliminary","");
   leg3->AddEntry(h,"p+Pb #sqrt{s_{NN}}= 5.02 TeV","");
  
   
   
   hRatio->Draw();  
   l->Draw("same");
   leg3->Draw("same");
 

   TCanvas *cMass = new TCanvas("cMass","Mass",600,600,600,600);
   hMass->SetXTitle("p_{T} (in GeV)");
   hMass->SetYTitle("M_{#phi} (GeV/c^{2})");
   hMass->GetXaxis()->CenterTitle();
   hMass->GetYaxis()->CenterTitle();
   hMass->SetTitleOffset(1.65,"Y");
   hMass->SetAxisRange(1.018,1.022,"Y");
   hMass->Draw(); 
   
	TLegend *leg4 = myLegend(0.55,0.72,0.96,0.82);
   leg4->AddEntry(hRatio,"MC HIJING (1.8M events)","");
   leg4->AddEntry(h,"CMS Preliminary","");
   leg4->AddEntry(hMass,"p+Pb #sqrt{s_{NN}}= 5.02 TeV","");
   leg4->Draw("same");
  

   TLine *l2 = new TLine(0,1.019445,200,1.019445);
   l2->SetLineStyle(2);
   l2->Draw("same");

   //cMass->SaveAs("phiFigure/phiMass.C");
   cMass->SaveAs("phiMass.gif");
   //cMass->SaveAs("phiFigure/phiMass.eps");*/
   outf->Write();

}
