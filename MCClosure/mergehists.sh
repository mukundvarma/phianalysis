if [ $# -ne 3 ]
then
  echo "Usage: ./mergehists.sh <tag> <nmin> <nmax>"
  exit 1
fi

echo | awk -v tag=$1 -v nmin=$2 -v nmax=$3 '{print "hadd -f /net/hisrv0001/home/mukund/scratch/corrhists/"tag"/merged/"tag"_nmin"nmin"_nmax"nmax".root /net/hisrv0001/home/mukund/scratch/corrhists/"tag"/nmin"nmin"_nmax"nmax"/*.root "}' | bash
echo "merging successful"
