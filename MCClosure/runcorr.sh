if [ $# -ne 6 ]
then
  echo "Usage: ./runcorr.sh <condor_iteration> <trackqual> <file-list> <tag> <nmin> <nmax>"
  exit 1
fi

echo | awk -v i=$1 -v trkq=$2 -v flist=$3 -v tag=$4 -v nmin=$5 -v nmax=$6 '{print "./runcorr.exe "i" "trkq" "flist" "tag" "nmin" "nmax" "}' | bash
sleep $[$RANDOM % 600]s

echo | awk -v tag=$4 -v nmin=$5 -v nmax=$6 '{print "scp *.root mukund@hidsk0001:/export/d00/scratch/mukund/corrhists/"tag"/nmin"nmin"_nmax"nmax"/ "}' | bash
rm *.root

echo `hostname`
echo "job done successfully"